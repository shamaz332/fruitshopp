@section('head')
<script src="{{asset("js/jquery.min.js")}}"></script>
        <script src="{{asset("js/jquery.waterwheelCarousel.js")}}"</script>
        <script src="{{asset("proper.js")}}"></script>
        <script src="{{asset("js/bootstrap.min.js")}}"></script>
        <script type="{{asset("text/javascript")}}">
            $(document).ready(function() {
                var carousel = $("#carousels").waterwheelCarousel({
                    flankingItem: 3
                });
                $('#prev').bind('click', function() {
                    carousel.prev();
                    return false;
                });
                $('#next').bind('click', function() {
                    carousel.next();
                    return false;
                });
            });

        </script>


        @show