
    
    @section ('header')
    <!--this is top bar-->

        <div class="row pad-normal">
            <div class="col-md-6 text-left languages d-none d-md-block">
                <span><a href="#"><i class="fa fa-language"></i>English</a></span>
                <span><a href="#"><i class="fa fa-money"></i>Rupee</a></span>
            </div>
            <div class="col-md-6 text-right Accounts">
                <span><a href="admin/index.php"><i class="fa fa-user"></i>Admin account</a></span>
                <span><a href="admin/register.php"><i class="fa fa-sign-in"></i>Register</a></span>
                <span class="d-none d-md-block"><a href=""><i class="fa fa-check-square-o"></i>Checkout</a></span>
            </div>
        </div>
    </div>
    <!--end of top bar-->
    <div class="container-fluid logo_bar">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 text-center">
                    <form class="form-inline my-2 my-lg-0">
                        <div class="search">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search">
                            <!-- <a href=""><i class="fa fa-search"></i></a>1122
                             -->

                        </div>
                    </form>
                </div>
                <div class="col-md-4 col-sm-12 text-center ">
                    <div class="logo_img">
                        <img src="images/logo.png" alt="">
                    </div>
                </div>
                <div class="col-md-4 pull-right d-none d-md-block">
                    <div class="card_details ">
                        <a href=""><i class="fa fa-credit-card"></i>Cart-Details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--    start of nav bar -->
    <div class="container margin-nav">

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-11 text-uppercase">

                <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-top:40px;">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
                    <a class="navbar-brand " id="actives" href="index.html">Fruit Organic</a>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a class="nav-link" href="shopnow.php">Fresh Fruit <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="dryfruit.php">Dry Fruit</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Categories</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#locate">Locate Us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="aboutus.html">About Us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#contactus">Contact Us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="shopnow.php">Order Now</a>
                            </li>
                        </ul>

                    </div>
                </nav>
            </div>



        
@show