@section('footer')
        <!--    footer of the web-->
        <div class="container-fluid  bg-color">
            <div class="container">
                <div class="row margin1">
                    <div class="col-md-4">
                        <h1 id="aboutus">ABOUT US</h1> <br>
                        <img src="images/about.png" class="img-fluid" alt="">
                        <p>Our products are freshly harvested, washed ready for box and finally.</p>

                    </div>
                    <div class="col-md-4">
                        <h1>INFORMMATION</h1>
                        <ul>
                            <li><a href="">New Products</a></li>
                            <li><a href="">Top Sellers</a></li>
                            <li><a href="">Our Blog</a></li>
                            <li><a href="">About Our Shop</a></li>
                            <li><a href="">About us</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h1 id="contactus">CONTACT US</h1>

                        <p> <i class="fa fa-home"></i>Our business address is fruit organic shop,Main Market.</p>
                        <p><i class="fa fa-mobile"></i>+923000000000</p>
                        <p><i class="fa fa-envelope"></i>fruitorganic@gmail.com</p>
                        <p>For Product Registration and general enquires please <a href="mailto:fruitorganic@gmail.com" style="color: #66CC33;">contact us</a>.</p>
                    </div>

                </div>
                <hr>
                <div class="row margin1 img">
                    <div class="col-md-6">
                        <span>FOLLOW US</span>
                        <a href=""> <img src="images/fa.svg" alt=""></a>
                        <a href="">  <img src="images/Twitter-icon.png" alt=""></a>
                        <a href=""><img src="images/fa.svg" alt=""></a>
                        <a href=""> <img src="images/2000px-Google_plus_icon.svg.png" alt=""></a>

                    </div>
                    <div class="col-md-6 text-right">
                        <span>PAYMENT METHOD</span>
                        <a href="">
                        <img src="images/pay1.png" alt="">
                        <img src="images/pay2.png" alt="">
                        <img src="images/pay3.png" alt="">
                        <img src="images/pay4.png" alt="">
                    </a>
                    </div>

                </div>

            </div>
        
        <div class="container margin ">
            <div class="row text-center">
                <div class="col-md-12">
                    <p> Copyright © 2020 Fruit Store - All Rights Reserved.</p>
                    <a href=""><span style="border-right:2px solid #66CC33;padding: 5px;">Privacy Policy</span> Term  Conditions</a>
                </div>
            </div>
        </div>
@show